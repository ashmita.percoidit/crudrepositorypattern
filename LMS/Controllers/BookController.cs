﻿using LMS.Data;
using LMS.Models;
using LMS.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Controllers
{
    public class BookController : Controller
    {
      
        private readonly IRepository bookrepo;

        public IActionResult Index()
        {
            IEnumerable<Book> books = bookrepo.GetAllBooks();
            return View(books);
        }

        public BookController(IRepository bookrepo)
        {
            this.bookrepo = bookrepo;
        }
        public IActionResult Create()
        {
            Book book = new Book();
            return View(book);
        }

        [HttpPost]
        public IActionResult Create(Book book)
        {
            try
            {
                book = bookrepo.Insert(book);
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction(nameof(Index));
        }




        
        public IActionResult Details(int id)
        {
            Book book = bookrepo.GetBook(id);
            return View(book);
        }





        public IActionResult Edit(int id)
        {
            Book book = bookrepo.GetBook(id);
            return View(book);
        }
        [HttpPost]
        public IActionResult Edit(Book book)
        {
            try
            {
                book = bookrepo.Update(book);
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction(nameof(Index));
        }






        public IActionResult Delete (int id)
        {
            Book book = bookrepo.GetBook(id);
            return View(book);
        }
        [HttpPost]
        public IActionResult Delete(Book book)
        {
            try
            {
                book = bookrepo.Delete(book);
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction(nameof(Index));
        }






      
    }
}
