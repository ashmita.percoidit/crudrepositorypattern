﻿using LMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Repository
{
   public interface IRepository
    {
        IEnumerable<Book> GetAllBooks();
        Book GetBook(int id);
        Book Insert(Book book);
        Book Update(Book book);
        Book Delete(Book book);

    }
}
