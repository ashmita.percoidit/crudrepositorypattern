﻿using LMS.Data;
using LMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Repository
{
    public class BookRepository : IRepository
    {
        private readonly BookDbContext context;

        public BookRepository(BookDbContext context)
        {
            this.context = context;
        }
        public Book GetBook(int id)
        {
            Book book = context.Books.Where(x => x.BookID == id).FirstOrDefault();
            return book;
        }

        public IEnumerable<Book> GetAllBooks()
        {
            List<Book> books = context.Books.ToList();
            return books;
        }

        public Book Insert(Book book)
        {
            Book books = new Book();
            books.Name = book.Name;
            books.Faculty = book.Faculty;
            books.Status = book.Status;
            books.Author = book.Author;
            books.PublishedDate = book.PublishedDate;
            books.CreatedTS = DateTime.Now;
            books.CreatedBy = book.CreatedBy;
            context.Add(books);
            context.SaveChanges();
            return books;
        }

        public Book Update(Book book)
        {
            book.ModifiedTS = DateTime.Now;
            context.Update(book);
            context.SaveChanges();
            return book;
        }

        public Book Delete(Book book)
        {
            context.Remove(book);
            context.SaveChanges();
            return book;
        }
    }
}
