﻿using LMS.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Data
{
    public class BookDbContext:DbContext
    {
        public BookDbContext(DbContextOptions options):base(options)
        {

        }
        public virtual DbSet<Book> Books { get; set; }
    }
}
