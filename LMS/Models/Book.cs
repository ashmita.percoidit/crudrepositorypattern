﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class Book
    {
        public int BookID { get; set; }
        public string Name { get; set; }
        public string Faculty { get; set; }
        public string Status { get; set; }
        public string Author { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime CreatedTS { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedTS { get; set; }
        public int ModifiedBy { get; set; }

    }
}
